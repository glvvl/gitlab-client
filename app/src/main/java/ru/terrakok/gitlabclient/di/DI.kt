package ru.terrakok.gitlabclient.di

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 09.07.17.
 */
object DI {
    // Single scopes
    const val APP_SCOPE = "app scope"
    const val SERVER_SCOPE = "server scope"
}